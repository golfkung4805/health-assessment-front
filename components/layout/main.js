import React, { useState, useEffect } from "react";
import { getUser, signIn } from '../../lib/user.js';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MainMenu = (props) => {
  const [userRegis, setUserRegis] = useState(false);

  useEffect(()=>{
    const user = getUser();
    // console.log("user -> ",user);
    if(user['profile'] === null) {
      
    }else{
      setUserRegis(user.profile.fullname)
    }

  },[]);

  const onSave = async () => {
    const user = getUser();
    console.log("user -> ",user);
    if(user['profile'] === null) {
        const { value: formValues } = await Swal.fire({
            title: 'ลงทะเบียนเพื่อ ประเมิณผล',
            html:
                '<input type="text" placeholder="กรอก ชื่อ-นามสกุล" maxlength="20" id="swal-fullname" className="swal2-input"/>' +
                '<input type="text" placeholder="กรอก เบอร์โทร" maxlength="10" id="swal-tel" className="swal2-input"/>',
                // '<input type="datetime" name="birthday" id="swal-birthDay" class="swal2-input"/>',
            focusConfirm: false,
            preConfirm: () => {
                return [
                document.getElementById('swal-fullname').value,
                document.getElementById('swal-tel').value
                // document.getElementById('swal-birthDay').value
                ]
            }
        })

        if (formValues[0] !== '' && formValues[1] !== '') {
          setData({
              fullname:formValues[0],
              tel:formValues[1]
          });
      }

    }else{
    
    }
  };

  return (
    <div>
      <header>
        <nav className="navbar navbar-expand-lg navbar-light shadow-sm">
          <div className="container">
            <a className="navbar-brand" href="/">
              <span className="text-primary">การคัดกรองสุขภาพ</span> ผู้สูงอายุ
            </a>

            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupport"
              aria-controls="navbarSupport"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupport">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item active">
                  <a className="nav-link" href="/">
                    หน้าแรก
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/">
                    เกี่ยวกับเรา
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/">
                    วิดิโอสาธิตการดูแลผู้ป่วย
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/">
                    บริการ
                  </a>
                </li>
                {/* <li className="nav-item">
                  <a className="nav-link" href="contact.html">
                    ติดต่อเรา
                  </a>
                </li> */}
                <li className="nav-item">
                    {(userRegis === false ? 
                      <button className="btn btn-primary ml-lg-3" onClick={()=>{onSave()}}>
                        ลงทะเบียน
                      </button>
                    :
                      <button className="btn btn-primary ml-lg-3" >
                        ผู้ใช้ {userRegis}
                      </button>
                    )}
                    
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>

      {props.children}

      <footer className="page-footer">
        <div className="container">
            <div className="page-section banner-home bg-image" style={{borderRadius:20}} >
                <div className="container py-5 py-lg-0">
                <div className="row align-items-center">
                    <div className="col-lg-4 wow zoomIn">
                    <div className="img-banner d-none d-lg-block">
                        <img src="../img/mobile_app.png" alt=""/>
                    </div>
                    </div>
                    <div className="col-lg-8 wow fadeInRight">
                    <h1 className="font-weight-normal mb-3">เริ่มต้นการใช้งาน MSU Home care delivery ได้ที่นี่</h1>
                    <a href="#"><img src="../img/google_play.svg" alt=""/></a>
                    <a href="#" className="ml-2"><img src="../img/app_store.svg" alt=""/></a>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <br/>
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <center>
                        <p>Copyright &copy; 2021 by<a href="https://nu.msu.ac.th/main/" target="_blank"> คณะพยาบาลศาสตร์ มหาวิทยาลัยมหาสารคาม</a>. </p>
                    </center>
                </div>
            </div>
        </div>
      </footer>
    </div>
  );
};

export default MainMenu;
