// src/context/state.js
import { createContext, useContext } from 'react';
import  { useState } from 'react';

const AppContext = createContext();

export function AppWrapper({ children }) {

  const [user, setUser] = useState({
    fullname:null,
    tel:null,
    birthDay:null
  });

  let sharedState = {
    user: {
      data:user,
      update:setUser
    }
  }

  return (
    <AppContext.Provider value={sharedState}>
      {children}
    </AppContext.Provider>
  );
}

export function useAppContext() {
  return useContext(AppContext);
}