export const commas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const monthName = (_number) => {

    switch (_number) {

        case "01":
            return "ม.ค.";
            break;
        case "02":
            return "ก.พ.";
            break;
        case "03":
            return "มี.ค.";
            break;
        case "04":
            return "เม.ย.";
            break;
        case "05":
            return "พ.ค.";
            break;
        case "06":
            return "มิ.ย.";
            break;
        case "07":
            return "ก.ค.";
            break;
        case "08":
            return "ส.ค.";
            break;
        case "09":
            return "ก.ย.";
            break;
        case "10":
            return "ต.ค.";
            break;
        case "11":
            return "พ.ย.";
            break;
        case "12":
            return "ธ.ค.";
            break;
    
        default:
            return "~";
            break;
    }
}

export const formatYear = (_number) => {
    return ( (parseInt(_number) + 543).toString().slice(2) );
}

