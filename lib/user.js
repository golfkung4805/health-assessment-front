
export const signIn = ( fullname, tel, birthDay) => {
    localStorage.setItem("profile", JSON.stringify({ fullname: fullname, tel: tel, birthDay: birthDay }));
    return {status: true}
}

export const setData = ( obj ) => {
    localStorage.setItem("profile", JSON.stringify(obj));
    return {status: true}
}

export const signUp = () => {
        
}

export const signOut = () => {
    localStorage.removeItem('profile');
}

export const getUser = () => {
    const profile = JSON.parse(localStorage.getItem('profile'));
    return {profile: profile};
}

