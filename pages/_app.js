import '../public/styles/custom.css';
import "rsuite/dist/rsuite.min.css";


const MyApp = ({ Component, pageProps }) => {
  return <Component {...pageProps} />
}

export default MyApp
