import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    return (
      <Html lang="th">
        <Head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
          <meta name="referrer" content="origin"/>
          {/* <meta property="og:title" content="การคัดกรองสุขภาพผู้สูงอายุ">
            
          <meta property="og:type" content="website">
          <meta property="og:url" content="https://sites.google.com/msu.ac.th/msuscreening/%E0%B8%AB%E0%B8%99%E0%B8%B2%E0%B9%81%E0%B8%A3%E0%B8%81">
          <meta property="og:description" content="
          แบบทดสอบความเสี่ยงประเมินโควิด 19">
          <meta itemprop="name" content="การคัดกรองสุขภาพผู้สูงอายุ">
          <meta itemprop="description" content="
          แบบทดสอบความเสี่ยงประเมินโควิด 19">
          <meta itemprop="url" content="https://sites.google.com/msu.ac.th/msuscreening/%E0%B8%AB%E0%B8%99%E0%B8%B2%E0%B9%81%E0%B8%A3%E0%B8%81">
          <meta itemprop="thumbnailUrl" content="https://lh3.googleusercontent.com/slytpwZBdvTHkJsKd5i_R3Bua0a_lizl-JLNEeLp6sXaIhTdYwLUGHFZMDmLDBHiXD4DPR6wLie8HRzADXhSaK0=w1280">
          <meta itemprop="image" content="https://lh3.googleusercontent.com/slytpwZBdvTHkJsKd5i_R3Bua0a_lizl-JLNEeLp6sXaIhTdYwLUGHFZMDmLDBHiXD4DPR6wLie8HRzADXhSaK0=w1280">
          <meta itemprop="imageUrl" content="https://lh3.googleusercontent.com/slytpwZBdvTHkJsKd5i_R3Bua0a_lizl-JLNEeLp6sXaIhTdYwLUGHFZMDmLDBHiXD4DPR6wLie8HRzADXhSaK0=w1280">
          <meta property="og:image" content="https://lh3.googleusercontent.com/slytpwZBdvTHkJsKd5i_R3Bua0a_lizl-JLNEeLp6sXaIhTdYwLUGHFZMDmLDBHiXD4DPR6wLie8HRzADXhSaK0=w1280"> */}

          <link rel="shortcut icon" href="../favicon.ico" />
          <link rel="stylesheet" href="/styles/maicons.css"/>
          <link rel="stylesheet" href="/styles/all.css"/>
          <link rel="stylesheet" href="/styles/bootstrap.css"/>
          <link rel="stylesheet" href="/styles/theme.css"/>

        </Head>
        <body>
          <div id="fb-root"></div>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
