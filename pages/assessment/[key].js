import React, { useState } from "react";
import { useRouter } from 'next/router'
import Head from "next/head";
import Layout from "../../components/layout/main";
import { useAppContext } from '../../context/state';
import { getUser, setData } from '../../lib/user.js';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import Steps from 'rsuite/Steps';
import variable from '../../variable';

const Assessment = (props) => {

  const router = useRouter();
  let _useAppContext = useAppContext();
  const [finalScore, setFinalScore] = useState(null);
  const [modal, setModal] = useState(false);
  const [step, setStep] = useState(0);
  const [form, setForm] = useState(variable[props.params].form.list);
  // console.log("form -> ",form);
  const onSave = async () => {
    const user = getUser();
    // console.log("user -> ",user);

    if(user['profile'] === null) {
        const { value: formValues } = await Swal.fire({
            title: 'ลงทะเบียนเพื่อ ประเมิณผล',
            html:
                '<input type="text" placeholder="กรอก ชื่อ-นามสกุล" maxlength="20" id="swal-fullname" className="swal2-input"/>' +
                '<input type="text" placeholder="กรอก เบอร์โทร" maxlength="10" id="swal-tel" className="swal2-input"/>',
                // '<input type="datetime" name="birthday" id="swal-birthDay" className="swal2-input"/>',
            focusConfirm: false,
            preConfirm: () => {
                return [
                document.getElementById('swal-fullname').value,
                document.getElementById('swal-tel').value
                // document.getElementById('swal-birthDay').value
                ]
            }
        })

        if (formValues[0] !== '' && formValues[1] !== '') {
            setData({
                fullname:formValues[0],
                tel:formValues[1]
            });
        }

    }else{
        console.log("form -> ",form);
        let validateChoice = form.find((x)=> x.score === null);
        if(validateChoice === undefined){
            const sum = form.reduce((a, b) => {
                return {score: a.score + b.score};
            });
            setFinalScore(sum.score);
        }else{
            Swal.fire({
                icon: 'warning',
                title: `คุณยังไม่ได้ตอบคำถามข้อที่ ${validateChoice.key}`,
                confirmButtonText: 'ตกลง'
                // text: `คุณยังไม่ได้เลือกคำตอบข้อที่ ${validateChoice.key}`,
            })
            setStep(validateChoice.key-1);
        }
    }
  };

  const onClickStep = (_page) => {
    setStep(_page)
  }

  const onChangeRadio = (_step, _index, _event) => {

    const choices = [
      ...form[_step].choice.slice(0, _index).map((row) => Object.assign(row,{checked:false})),
      { ...Object.assign(form[_step].choice[_index],{checked:true}) },
      ...form[_step].choice.slice(_index+1).map((row) => Object.assign(row,{checked:false}))
    ]

    setForm([
      ...form.slice(0, _step),
      { ...Object.assign(form[_step],{choice: choices, score: parseInt(_event.target.value)}) },
      ...form.slice(_step+1)
    ]);
    // console.log("=>",[
    //   { ...Object.assign(...form.slice(0, _step), {checked:false}) },
    //   { ...Object.assign(form[_step],{choice: choices, score: parseInt(_event.target.value)}) },
    //   { ...Object.assign(...form.slice(_step+1), {checked:false}) }
    // ])

  }

  return (
    <Layout>
      <Head>
        <title>{variable[props.params].title}</title>
      </Head>

      <div className="page-hero bg-primary-soft" style={{ height: "100%" }}>
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-12 py-12 wow fadeInUp m-1-0">
              <div className="card-blog" style={{ maxWidth: 1024 }}>
                <div className="body">

                  <h3 className="post-title">
                    { variable[props.params].form.title }
                  </h3>

                  {finalScore === null && (<>
                    <div className="blog-details">
                      <div className="row">
                        <div className="col-12">
                          <div style={{width:'100%', overflow:'auto'}}>
                            <Steps small current={step} style={{margin:'2rem 0rem 1rem 0rem'}}>
                              {form.map((row, index)=>{
                                return <Steps.Item key={index} label={`ข้อ ${(index + 1)}`} onClick={()=>{onClickStep(index)}}/>;
                              })}
                            </Steps>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12">
                          <div className="fade-in-image" style={{ margin: "1rem .2rem" }}>
                              <h3 className="question-title">{form[step].question}</h3>
                              <div style={{ margin: "1rem 1.5rem" }}>

                                {form[step].choice.map((row, index)=>{
                                  return (
                                    <label key={`choice${index}`} style={{ margin: ".2rem 0rem" }} className="container-radio">{row.text}
                                      <input onChange={(e)=>{onChangeRadio(step,index,e)}} type="radio" value={row.point} checked={row.checked} id={`${row.group}_${row.key}`} name={row.group}/>
                                      <span className="checkmark"></span>
                                    </label>
                                  )
                                })}
              
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="blog-details">
                      <div style={{ margin: "1rem .2rem", textAlign: "center" }}>
                          
                          {(step === (form.length - 1) ?
                          <center>
                              <button style={{margin: '0rem .2rem'}} className="btn btn-danger ml-lg-3" onClick={()=>{ if(step > 0){setStep(step - 1)} }}>
                                กลับ
                              </button>
                              <button style={{margin: '0rem .2rem'}} className="btn btn-primary ml-lg-3" onClick={()=>{onSave()}}>
                                ประมวลผล
                              </button>
                          </center>
                          :
                            <center>
                              <button style={{margin: '0rem .2rem'}} className="btn btn-danger ml-lg-3" onClick={()=>{ if(step > 0){setStep(step - 1)} }}>
                                กลับ
                              </button>
                              <button style={{margin: '0rem .2rem'}} className="btn btn-info ml-lg-3" onClick={()=>{ if(step !== (form.length - 1)){setStep(step + 1)} }}>
                                ถัดไป
                              </button>
                            </center>
                          )}
                      </div>
                    </div>
                  </>)}
                  {/* <hr /> */}
                  {finalScore !== null && (<>
                  <div className="blog-details">
                    <div style={{ margin: "1rem .2rem" }}>
                      <h4>
                        สรุปผลรวมคะแนน{" "}
                        <strong style={{ color: "#03a882", margin:'0 .4rem' }}>
                          { finalScore } 
                        </strong>
                        { (finalScore !== null && variable[props.params].condition(finalScore).name ) }
                      </h4>
                    </div>

                    {/* <div style={{ margin: ".3rem 1.5rem" }}>
                      <p style={{ margin: ".2rem 0rem", fontSize:17 }}>
                        <span style={{ color: "#03a882" }}>●</span>{" "}
                        กลุ่มติดสังคม มีผลรวมคะแนน BADL ตั้งแต่ 12 คะแนนขึ้นไป
                      </p>
                      <p style={{ margin: ".2rem 0rem", fontSize:17 }}>
                        <span style={{ color: "#03a882" }}>●</span> กลุ่มติดบ้าน
                        มีผลรวมคะแนน ADL อยู่ในช่วง 5-11 คะแนน
                      </p>
                      <p style={{ margin: ".2rem 0rem", fontSize:17 }}>
                        <span style={{ color: "#03a882" }}>●</span>{" "}
                        กลุ่มติดเตียง มีผลรวมคะแนน ADL อยู่ในช่วง 0 - 4 คะแนน
                      </p>
                    </div> */}
                    
                  </div>
                  </>)}
                  <hr />

                  <div className="site-info">
                    <div className="avatar mr-2">
                      <div className="avatar-img">
                        <img src="../logo.png" alt="" />
                      </div>
                      <span>คณะพยาบาลศาสตร์ มหาวิทยาลัยมหาสารคาม</span>
                    </div>
                    <span></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default Assessment;

export async function getServerSideProps(context) {
  
  return {
    props:{
      params:context['params']['key']
    }
  }
}
