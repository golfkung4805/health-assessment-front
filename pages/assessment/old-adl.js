import React, { useState } from "react";
import Head from "next/head";
import Layout from "../../components/layout/main";
import { useAppContext } from '../../context/state';
import { getUser, setData } from '../../lib/user.js';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const Assessment = (props) => {
  let _useAppContext = useAppContext();
  const [finalScore, setFinalScore] = useState(0);
  const [modal, setModal] = useState(false);

  const question = [
    {
      id: 1,
      title:
        "1. Feeding (รับประทานอาหารเมื่อเตรียมสำรับไว้ให้เรียบร้อยต่อหน้า)",
      list: [
        {
          seq: 1,
          title: "ไม่สามารถตักอาหารเข้าปากได้ ต้องมีคนป้อนให้",
          score: 0,
          value: null,
        },
        {
          seq: 2,
          title:
            "ตักอาหารเองได้แต่ต้องมีคนช่วย เช่น ช่วยให้ช้อนตักเตรียมไว้ให้หรือตัดเป็นเล็กๆ ไว้ล่วงหน้า",
          score: 0,
          value: null,
        },
        {
          seq: 3,
          title: "ตักอาหารและช่วยตัวเองได้เป็นปกติ",
          score: 0,
          value: null,
        },
      ],
    },
    {
      id: 2,
      title:
        "2. Grooming (ล้างหน้า หวีผม แปรงฟัน โกนหนวด ในระยะเวลา 24 - 48 ชั่วโมงที่ผ่านมา)",
      list: [
        {
          seq: 1,
          title: "ต้องการความช่วยเหลือ",
          score: 0,
          value: null,
        },
        {
          seq: 2,
          title: "ทำเองได้ (รวมทั้งที่ทำได้เองถ้าเตรียมอุปกรณ์ไว้ให้)",
          score: 0,
          value: null,
        },
      ],
    },
    {
      id: 3,
      title: "3. Transfer (ลุกนั่งจากที่นอน หรือจากเตียงไปยังเก้าอี้) ",
      list: [
        {
          seq: 1,
          title:
            "ไม่สามารถนั่งได้ (นั่งแล้วจะล้มเสมอ) หรือต้องใช้คนสองคนช่วยกันยกขึ้น",
          score: 0,
          value: null,
        },
        {
          seq: 2,
          title:
            "ต้องการความช่วยเหลืออย่างมาก เช่น ต้องใช้คนที่แข็งแรงหรือมีทักษะ 1 คน หรือใช้คนทั่วไป 2 คน พยุงหรือดันขึ้นมาจึงจะนั่งอยู่ได้",
          score: 0,
          value: null,
        },
        {
          seq: 3,
          title:
            "ต้องการความช่วยเหลือบ้าง เช่นบอกให้ทําตาม หรือช่วยพยุงเล็กน้อย หรือต้องมีคนดูแลเพื่อความปลอดภัย",
          score: 0,
          value: null,
        },
        {
          seq: 4,
          title: "ทําได้เอง",
          score: 0,
          value: null,
        },
      ],
    },
    {
      id: 4,
      title: "4. Toilet use (ใช้ห้องน้ํา)",
      list: [
        {
          seq: 1,
          title: "ช่วยตัวเองไม่ได้",
          score: 0,
          value: null,
        },
        {
          seq: 2,
          title:
            "ทําเองได้บ้าง (อย่างน้อยทําความสะอาดตัวเองได้หลังจากเสร็จธุระ) แต่ต้องการความช่วยเหลือในบางสิ่ง",
          score: 0,
          value: null,
        },
        {
          seq: 3,
          title:
            "ต้องการความช่วยเหลือบ้าง เช่นบอกให้ทําตาม หรือช่วยพยุงเล็กน้อย หรือต้องมีคนดูแลเพื่อความปลอดภัย",
          score: 0,
          value: null,
        },
        {
          seq: 4,
          title:
            "ช่วยตัวเองได้ดี (ขึ้นนั่งและลงจากโถส้วมเองได้ ทําความสะอาดได้เรียบร้อยหลังจากเสร็จธุระถอดใส่ เสื้อผ้าได้เรียบร้อย)",
          score: 0,
          value: null,
        },
      ],
    },
    {
      id: 5,
      title: "5. Mobility (การเคลื่อนที่ภายในห้องหรือ บ้าน)",
      list: [
        {
          seq: 1,
          title: "เคลื่อนที่ไปไหนไม่ได้",
          score: 0,
          value: null,
        },
        {
          seq: 2,
          title:
            "ต้องใช้รถเข็นช่วยตัวเองให้เคลื่อนที่ได้เอง (ไม่ต้องมีคนเข็นให้) และจะต้องเข้าออกมุมห้องหรือประตูได้",
          score: 0,
          value: null,
        },
        {
          seq: 3,
          title:
            "เดินหรือเคลื่อนที่โดยมีคนช่วย เช่น พยุง หรือบอกให้ทําตามงหรือต้องให้ความสนใจดูแลเพื่อความปลอดภัย",
          score: 0,
          value: null,
        },
        {
          seq: 4,
          title: "เดินหรือเคลื่อนที่ได้เอง",
          score: 0,
          value: null,
        },
      ],
    },
    {
      id: 6,
      title: "6. Dressing (การสวมใส่เสื้อผ้า)",
      list: [
        {
          seq: 1,
          title: "ต้องมีคนสวมใส่ให้ ช่วยตัวเองแทบไม่ได้หรือได้น้อย",
          score: 0,
          value: null,
        },
        {
          seq: 2,
          title: "ช่วยตัวเองได้ประมาณร้อยละ 50 ที่เหลือต้องมีคนช่วย",
          score: 0,
          value: null,
        },
        {
          seq: 3,
          title:
            "ช่วยตัวเองได้ดี (รวมทั้งการติดกระดุม รูดซิบ หรือใช้เสื้อผ้าที่ดัดแปลงให้เหมาะสมก็ได้)",
          score: 0,
          value: null,
        },
      ],
    },
    {
      id: 7,
      title: "7. Stairs (การขึ้นลงบันได 1 ชั้น)",
      list: [
        {
          seq: 1,
          title: "ไม่สามารถทําได้ ",
          score: 0,
          value: null,
        },
        {
          seq: 2,
          title: "ต้องการคนช่วย",
          score: 0,
          value: null,
        },
        {
          seq: 3,
          title:
            "ขึ้นลงได้เอง (ถ้าต้องใช้เครื่องช่วยเดิน เช่น walker จะต้องเอาขึ้นลงได้ด้วย)",
          score: 0,
          value: null,
        },
      ],
    },
    {
      id: 8,
      title: "8. Bathing (การอาบน้ํา)",
      list: [
        {
          seq: 1,
          title: "ต้องมีคนช่วยหรือทําให้",
          score: 0,
          value: null,
        },
        {
          seq: 2,
          title: "อาบน้ําเองได้",
          score: 0,
          value: null,
        },
      ],
    },
    {
      id: 9,
      title: "9. Bowels (การกลั่นการถ่ายอุจจาระในระยะ 1 สัปดาห์ที่ผ่านมา)",
      list: [
        {
          seq: 1,
          title: "กลั้นไม่ได้ หรือต้องการการสวนอุจจาระอยู่เสมอ",
          score: 0,
          value: null,
        },
        {
          seq: 2,
          title: "กลั้นไม่ได้บางครั้ง (เป็นน้อยกว่า 1 ครั้งต่อสัปดาห์)",
          score: 0,
          value: null,
        },
        {
          seq: 3,
          title: "กลั้นได้เป็นปกติ",
          score: 0,
          value: null,
        },
      ],
    },
    {
      id: 10,
      title: "10. Bladder (การกลั้นปัสสาวะในระยะ 1 สัปดาห์ที่ผ่านมา)",
      list: [
        {
          seq: 1,
          title: "กลั้นไม่ได้ หรือใส่สายสวนปัสสาวะแต่ไม่สามารถดูแลเองได้",
          score: 0,
          value: null,
        },
        {
          seq: 2,
          title: "กลั้นไม่ได้บางครั้ง (เป็นน้อยกว่าวันละ 1 ครั้ง)",
          score: 0,
          value: null,
        },
        {
          seq: 3,
          title: "กลั้นได้เป็นปกติ",
          score: 0,
          value: null,
        },
      ],
    },
  ];

  const onSave = async () => {
    const user = getUser();
    console.log("user -> ",user);
    if(user['profile'] === null) {
        const { value: formValues } = await Swal.fire({
            title: 'ลงทะเบียนเพื่อ ประเมิณผล',
            html:
                '<input type="text" placeholder="กรอก ชื่อ-นามสกุล" maxlength="20" id="swal-fullname" class="swal2-input"/>' +
                '<input type="text" placeholder="กรอก เบอร์โทร" maxlength="10" id="swal-tel" class="swal2-input"/>',
                // '<input type="datetime" name="birthday" id="swal-birthDay" class="swal2-input"/>',
            focusConfirm: false,
            preConfirm: () => {
                return [
                document.getElementById('swal-fullname').value,
                document.getElementById('swal-tel').value
                // document.getElementById('swal-birthDay').value
                ]
            }
        })

        if (formValues[0] !== '' && formValues[1] !== '') {
            setData({
                fullname:formValues[0],
                tel:formValues[1]
            });
        }

    }else{
        setFinalScore(15)
    }
  };

  return (
    <Layout>
      <Head>
        <title>แบบประเมิน ADL</title>
      </Head>

      <div className="page-hero bg-primary-soft" style={{ height: "100%" }}>
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-12 py-12 wow fadeInUp m-1-5">
              <div className="card-blog" style={{ maxWidth: 768 }}>
                <div className="body">
                  <h5 className="post-title">
                    <a style={{ fontSize: 22 }}>
                      แบบประเมินความสามารถ ในการดำเนินชีวิตประจำวัน (ADL)
                    </a>
                  </h5>

                  <div className="blog-details">
                    {question.map((row, index) => {
                      return (
                        <div
                          key={`question${row.id}`}
                          style={{ margin: "1rem .2rem" }}
                        >
                          <h6>{row.title}</h6>
                          <div style={{ margin: ".3rem 1.5rem" }}>
                            {row.list.map((element, number) => {
                              return (
                                <p
                                  key={element.seq}
                                  style={{ margin: ".2rem 0rem" }}
                                >
                                  <input
                                    type="checkbox"
                                    id="vehicle3"
                                    name="vehicle3"
                                    value="Boat"
                                  />
                                  <span style={{ margin: ".0rem .5rem" }}>
                                    {element.title}
                                  </span>
                                </p>
                              );
                            })}
                          </div>
                        </div>
                      );
                    })}
                  </div>

                  <div className="blog-details">
                    <div style={{ margin: "1rem .2rem", textAlign: "center" }}>
                      <button className="btn btn-primary ml-lg-3" onClick={()=>{onSave()}}>
                        ประมวลผล
                      </button>
                    </div>
                  </div>

                  <hr />
                  <div className="blog-details">
                    <div style={{ margin: "1rem .2rem" }}>
                      <h6>
                        สรุปผลรวมคะแนน{" "}
                        <strong style={{ color: "#03a882" }}>
                          {finalScore}
                        </strong>
                      </h6>
                    </div>
                    <div style={{ margin: ".3rem 1.5rem" }}>
                      <p style={{ margin: ".2rem 0rem" }}>
                        <span style={{ color: "#03a882" }}>●</span>{" "}
                        กลุ่มติดสังคม มีผลรวมคะแนน BADL ตั้งแต่ 12 คะแนนขึ้นไป
                      </p>
                      <p style={{ margin: ".2rem 0rem" }}>
                        <span style={{ color: "#03a882" }}>●</span> กลุ่มติดบ้าน
                        มีผลรวมคะแนน ADL อยู่ในช่วง 5-11 คะแนน
                      </p>
                      <p style={{ margin: ".2rem 0rem" }}>
                        <span style={{ color: "#03a882" }}>●</span>{" "}
                        กลุ่มติดเตียง มีผลรวมคะแนน ADL อยู่ในช่วง 0 - 4 คะแนน
                      </p>
                    </div>
                  </div>
                  <hr />

                  <div className="site-info">
                    <div className="avatar mr-2">
                      <div className="avatar-img">
                        <img src="../logo.png" alt="" />
                      </div>
                      <span>คณะพยาบาลศาสตร์ มหาวิทยาลัยมหาสารคาม</span>
                    </div>
                    <span></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default Assessment;
