import Head from "next/head";
import Image from "next/image";
import Layout from "../components/layout/main"
import { assessments } from "../variable"

const Home = (props) => {

  return (
    <Layout>

      <Head>
        <title>การคัดกรองสุขภาพผู้สูงอายุ</title>
      </Head>

      <div
        className="page-hero bg-image overlay-dark"
        style={{ backgroundImage: "url(/img/bg_image_2.jpg)" }}
      >
        <div className="hero-section">
          <div className="container text-center wow zoomIn">
            <h3 className="subhead">การคัดกรองสุขภาพผู้สูงอายุ</h3>
            <h1 className="display-4">ด้วยตัวเองหรือโดยผู้ดูแล</h1>
            <a href="" className="btn btn-red">
              ดูบริการ
            </a>
          </div>
        </div>
      </div>

      <div className="page-section pb-0">
        <div className="container">
          { assessments.map((item, index)=>{
            return (
              <div className="row align-items-center" key={index}>
                <div className={`col-lg-6 py-3 wow fadeInUp order-${index%2} ${(index%2 === 1) && (`text-right`)}`}>
                  {/* <h1>แบบประเมินคัดกรอง <br/> ADL</h1> */}
                  <h1>{item.title}</h1>
                  {/* <p className="text-grey mb-4">ADL - Activities of Daily Living หรือการดำเนินกิจวัตรประจำวันของผู้สูงอายุ แสดงสัญลักษณะที่บอกถึงสภาพร่างกาย และระดับความสามารถในการทำกิจวัตรประจำวันของผู้สูงอายุ เช่นการขับถ่าย การกิน และการอาบน้ำ</p> */}
                  <p className="text-grey text-left mb-4">{item.desc}</p>
                  <a href={`/assessment/${item.key}`} className={`btn btn-primary`}>ประเมินเลย</a>
                </div>
                <div className={`col-lg-6 wow fadeInRight order-0`} data-wow-delay="400ms">
                  <div className="img-place custom-img-1">
                    <div style={{height:350}}></div>
                    {/* <img src="../img/adl.png" alt=""/> */}
                  </div>
                </div>
              </div>
            )
          }) }

        </div>
      </div>

      <div style={{backgroundColor:'#e4edeb'}} className="page-section">
        <div className="container">
          <h1 className="text-center mb-5 wow fadeInUp text-bold">บุคคากร ของเรา</h1>
          <div className="row mt-5">
            <div className="col-lg-4 py-2 wow zoomIn">
              <div className="card-doctor">
                <div className="header">
                  <img src="../img/doctors/doctor_1.jpg" alt=""/>
                  <div className="meta">
                    {/* <a href="#"><span className="mai-call"></span></a>
                    <a href="#"><span className="mai-logo-whatsapp"></span></a> */}
                  </div>
                </div>
                <div className="body">
                  <p className="text-xl mb-0">Dr. Stein Albert</p>
                  <span className="text-sm text-grey">Cardiology</span>
                </div>
              </div>
            </div>

            <div className="col-lg-4 py-2 wow zoomIn" >
              <div className="card-doctor">
                <div className="header">
                  <img src="../img/doctors/doctor_2.jpg" alt=""/>
                  <div className="meta">
                    {/* <a href="#"><span className="mai-call"></span></a>
                    <a href="#"><span className="mai-logo-whatsapp"></span></a> */}
                  </div>
                </div>
                <div className="body">
                  <p className="text-xl mb-0">Dr. Alexa Melvin</p>
                  <span className="text-sm text-grey">Dental</span>
                </div>
              </div>
            </div>

            <div className="col-lg-4 py-2 wow zoomIn">
              <div className="card-doctor">
                <div className="header">
                  <img src="../img/doctors/doctor_3.jpg" alt=""/>
                  <div className="meta">
                    {/* <a href="#"><span className="mai-call"></span></a> */}
                    {/* <a href="#"><span className="mai-logo-whatsapp"></span></a> */}
                  </div>
                </div>
                <div className="body">
                  <p className="text-xl mb-0">Dr. Stein Albert</p>
                  <span className="text-sm text-grey">Cardiology</span>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

    </Layout>
  );
};
export default Home;
