
const service = {
    async getAll() {
        return await fetch(`http://localhost:3005/api/department`).then(r => r.json());
    },
    async getById(_id) {
        return await fetch(`http://localhost:3005/api/department/${_id}`).then(r => r.json());
    }
}

export default service;