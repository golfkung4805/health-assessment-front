
const service = {
    async add(_obj) {
        return await fetch(`http://localhost:3005/api/employee`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(_obj)
        }).then(r => r.json());
    },
    async addBulk(_obj) {
        return await fetch(`http://localhost:3005/api/employee/bulk`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(_obj)
        }).then(r => r.json());
    },
    async getAll() {
        return await fetch(`http://localhost:3005/api/employee`).then(r => r.json());
    },
    async getById(_id) {
        return await fetch(`http://localhost:3005/api/employee/${_id}`).then(r => r.json());
    },
    async update(_id,_obj) {
        return await fetch(`http://localhost:3005/api/employee/${_id}`,{
            method: 'PATCH',
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify(_obj)
        }).then(r => r.json());
    },
    async del(_id) {
        return await fetch(`http://localhost:3005/api/service-type/${_id}`,{
            method: 'DELETE',
            headers: {
            'Content-Type': 'application/json'
            }
        }).then(r => r);
    },
}

export default service;