const adl = {
        key: "adl",
        title: "แบบประเมินคัดกรอง ADL",
        desc: "ADL - Activities of Daily Living หรือการดำเนินกิจวัตรประจำวันของผู้สูงอายุ แสดงสัญลักษณะที่บอกถึงสภาพร่างกาย และระดับความสามารถในการทำกิจวัตรประจำวันของผู้สูงอายุ เช่นการขับถ่าย การกิน และการอาบน้ำ",
        form: {
            title: "แบบประเมินความสามารถ ในการดำเนินชีวิตประจำวัน (ADL)",
            list: [
                {
                    key:1,
                    question: "1. Feeding (รับประทานอาหารเมื่อเตรียมสำรับไว้ให้เรียบร้อยต่อหน้า)",
                    choice: [
                        {
                            key:"choice1",
                            group: "adl_1",
                            text:"ไม่สามารถตักอาหารเข้าปากได้ ต้องมีคนป้อนให้",
                            point:0,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice2",
                            group: "adl_1",
                            text:"ตักอาหารเองได้แต่ต้องมีคนช่วย เช่น ช่วยให้ช้อนตักเตรียมไว้ให้หรือตัดเป็นเล็กๆ ไว้ล่วงหน้า",
                            point:1,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice3",
                            group: "adl_1",
                            text:"ตักอาหารและช่วยตัวเองได้เป็นปกติ",
                            point:2,
                            value: null,
                            checked: false
                        }
                    ],
                    score: null
                },
                {
                    key:2,
                    question: "2. Grooming (ล้างหน้า หวีผม แปรงฟัน โกนหนวด ในระยะเวลา 24 - 48 ชั่วโมงที่ผ่านมา)",
                    choice: [
                        {
                            key:"choice1",
                            group: "adl_2",
                            text:"ต้องการความช่วยเหลือ",
                            point:0,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice2",
                            group: "adl_2",
                            text:"ทำเองได้ (รวมทั้งที่ทำได้เองถ้าเตรียมอุปกรณ์ไว้ให้)",
                            point:1,
                            value: null,
                            checked: false
                        }
                    ],
                    score: null
                },
                {
                    key:3,
                    question: "3. Transfer (ลุกนั่งจากที่นอน หรือจากเตียงไปยังเก้าอี้)",
                    choice: [
                        {
                            key:"choice1",
                            group: "adl_3",
                            text:"ไม่สามารถนั่งได้ (นั่งแล้วจะล้มเสมอ) หรือต้องใช้คนสองคนช่วยกันยกขึ้น",
                            point:0,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice2",
                            group: "adl_3",
                            text:"ต้องการความช่วยเหลืออย่างมาก เช่น ต้องใช้คนที่แข็งแรงหรือมีทักษะ 1 คน หรือใช้คนทั่วไป 2 คน พยุงหรือดันขึ้นมาจึงจะนั่งอยู่ได้",
                            point:1,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice3",
                            group: "adl_3",
                            text:"ต้องการความช่วยเหลือบ้าง เช่นบอกให้ทําตาม หรือช่วยพยุงเล็กน้อย หรือต้องมีคนดูแลเพื่อความปลอดภัย",
                            point:2,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice4",
                            group: "adl_3",
                            text:"ทําได้เอง",
                            point:3,
                            value: null,
                            checked: false
                        }
                    ],
                    score: null
                },
                {
                    key:4,
                    question: "4. Toilet use (ใช้ห้องน้ํา)",
                    choice: [
                        {
                            key:"choice1",
                            group: "adl_4",
                            text:"ช่วยตัวเองไม่ได้",
                            point:0,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice2",
                            group: "adl_4",
                            text:"ทําเองได้บ้าง (อย่างน้อยทําความสะอาดตัวเองได้หลังจากเสร็จธุระ) แต่ต้องการความช่วยเหลือในบางสิ่ง",
                            point:1,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice3",
                            group: "adl_4",
                            text:"ช่วยตัวเองได้ดี (ขึ้นนั่งและลงจากโถส้วมเองได้ ทำความสะอาดได้เรียบร้อยหลังจากเสร็จธุระถอดใส่เสื้อผ้าได้เรียบร้อย)",
                            point:2,
                            value: null,
                            checked: false
                        }
                       
                    ],
                    score: null
                },
                {
                    key:5,
                    question: "5. Mobility (การเคลื่อนที่ภายในห้องหรือ บ้าน)",
                    choice: [
                        {
                            key:"choice1",
                            group: "adl_5",
                            text:"เคลื่อนที่ไปไหนไม่ได้",
                            point:0,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice2",
                            group: "adl_5",
                            text:"ต้องใช้รถเข็นช่วยตัวเองให้เคลื่อนที่ได้เอง (ไม่ต้องมีคนเข็นให้) และจะต้องเข้าออกมุมห้องหรือประตูได้",
                            point:1,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice3",
                            group: "adl_5",
                            text:"เดินหรือเคลื่อนที่โดยมีคนช่วย เช่น พยุง หรือบอกให้ทําตามงหรือต้องให้ความสนใจดูแลเพื่อความปลอดภัย",
                            point:2,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice4",
                            group: "adl_5",
                            text:"เดินหรือเคลื่อนที่ได้เอง",
                            point:3,
                            value: null,
                            checked: false
                        }
                    ],
                    score: null
                },
                {
                    key:6,
                    question: "6. Dressing (การสวมใส่เสื้อผ้า)",
                    choice: [
                        {
                            key:"choice1",
                            group: "adl_6",
                            text:"ต้องมีคนสวมใส่ให้ ช่วยตัวเองแทบไม่ได้หรือได้น้อย",
                            point:0,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice2",
                            group: "adl_6",
                            text:"ช่วยตัวเองได้ประมาณร้อยละ 50 ที่เหลือต้องมีคนช่วย",
                            point:1,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice3",
                            group: "adl_6",
                            text:"ช่วยตัวเองได้ดี (รวมทั้งการติดกระดุม รูดซิบ หรือใช้เสื้อผ้าที่ดัดแปลงให้เหมาะสมก็ได้)",
                            point:2,
                            value: null,
                            checked: false
                        }
                    ],
                    score: null
                },
                {
                    key:7,
                    question: "7. Stairs (การขึ้นลงบันได 1 ชั้น)",
                    choice: [
                        {
                            key:"choice1",
                            group: "adl_7",
                            text:"ไม่สามารถทําได้",
                            point:0,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice2",
                            group: "adl_7",
                            text:"ต้องการคนช่วย",
                            point:1,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice3",
                            group: "adl_7",
                            text:"ขึ้นลงได้เอง (ถ้าต้องใช้เครื่องช่วยเดิน เช่น walker จะต้องเอาขึ้นลงได้ด้วย)",
                            point:2,
                            value: null,
                            checked: false
                        }
                    ],
                    score: null
                },
                {
                    key:8,
                    question: "8. Bathing (การอาบน้ํา)",
                    choice: [
                        {
                            key:"choice1",
                            group: "adl_8",
                            text:"ต้องมีคนช่วยหรือทําให้",
                            point:0,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice2",
                            group: "adl_8",
                            text:"อาบน้ําเองได้",
                            point:1,
                            value: null,
                            checked: false
                        }
                    ],
                    score: null
                },
                {
                    key:9,
                    question: "9. Bowels (การกลั่นการถ่ายอุจจาระในระยะ 1 สัปดาห์ที่ผ่านมา)",
                    choice: [
                        {
                            key:"choice1",
                            group: "adl_9",
                            text:"กลั้นไม่ได้ หรือต้องการการสวนอุจจาระอยู่เสมอ",
                            point:0,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice2",
                            group: "adl_9",
                            text:"กลั้นไม่ได้บางครั้ง (เป็นน้อยกว่า 1 ครั้งต่อสัปดาห์)",
                            point:1,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice3",
                            group: "adl_9",
                            text:"กลั้นได้เป็นปกติ",
                            point:2,
                            value: null,
                            checked: false
                        }
                    ],
                    score: null
                },
                {
                    key:10,
                    question: "10. Bladder (การกลั้นปัสสาวะในระยะ 1 สัปดาห์ที่ผ่านมา)",
                    choice: [
                        {
                            key:"choice1",
                            group: "adl_10",
                            text:"กลั้นไม่ได้ หรือใส่สายสวนปัสสาวะแต่ไม่สามารถดูแลเองได้",
                            point:0,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice2",
                            group: "adl_10",
                            text:"กลั้นไม่ได้บางครั้ง (เป็นน้อยกว่าวันละ 1 ครั้ง)",
                            point:1,
                            value: null,
                            checked: false
                        },
                        {
                            key:"choice3",
                            group: "adl_10",
                            text:"กลั้นได้เป็นปกติ",
                            point:2,
                            value: null,
                            checked: false
                        }
                    ],
                    score: null
                }
            ]
        },
        condition:(_score)=>{
            
            if(_score <= 4){
                return {
                    type:'ADL',
                    length:'0 - 4',
                    name:'กลุ่มติดเตียง'
                }
            } else if(_score <= 11) {
                return {
                    type:'ADL',
                    length:'5 - 11',
                    name:'กลุ่มติดบ้าน'
                }
            }
            else if(_score >= 12) {
                return {
                    type:'ADL',
                    length:'5 - 11',
                    name:'กลุ่มติดสังคม'
                }
            }
            else {
                return null
            }
        }
}

const oks = {
    key: "oks",
    title: "แบบประเมินระดับความรุนแรงของโรคข้อเข่าเสื่อม OKS",
    desc: "OKS TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT TEXT",
    form: {
        title: "แบบประเมินระดับความรุนแรงของโรคข้อเข่าเสื่อม (OKS)",
        list: [
            {
                key:1,
                question: "1) ลักษณะอาการเจ็บปวดเข่า หรือข้อพับเข่าของท่าน",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_1",
                        text:"ไม่มีอาการ",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_1",
                        text:"อาการปวดลึกๆที่เข่าเล็กน้อย เฉพาะเวลาขยับตัวหรืออยู่ในบางทำเท่านั้น",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_1",
                        text:"หลังใช้งานนาน อาการปวดเข่ามากขึ้น พักแล้วดีขึ้น เป็นๆหายๆ",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_1",
                        text:"อาการปวดเข่าเพิ่มมากขึ้น ปวดนานขึ้น",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_1",
                        text:"อยู่เฉยๆก็ปวดมาก ขยับไม่ได้",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:2,
                question: "2) ท่านมีปัญหาเรื่องเข่าในการทำกิจวัตรประจำวัน เช่น การยืนอาบน้ำ ใส่เสื้อผ้า หรือไม่",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_2",
                        text:"ไม่มีปัญหา",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_2",
                        text:"มีอาการปวดเข่า/ข้อเข่าฝืดตึงขัดเล็กน้อย แต่น้อยมาก",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_2",
                        text:"มีอาการปวดเข่า/ข้อเข่าฝืดตึงเล็กน้อย แต่บ่อยครั้ง",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_2",
                        text:"เริ่มมีปัญหาทำด้วยความยากลำบาก",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_2",
                        text:"ไม่สามารถทำได้",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:3,
                question: "3) ท่านมีปัญหาเรื่องเข่า เมื่อก้าว ขึ้น-ลง รถ หรือรถประจำทาง หรือไม่",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_3",
                        text:"ไม่มีอาการใดๆ",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_3",
                        text:"มีอาการปวดเข่า/ข้อเข่าฝืดตึงขัดเล็กน้อย แต่น้อยมาก",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_3",
                        text:"มีอาการปวดเข่า/ข้อเข่าฝืดก้าวขึ้นลงได้ชัากว่าปกติ",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_3",
                        text:"มีอาการปวดเข่ามาก/ข้อเข่าฝืด ก้าวขึ้นลงด้วยความยากลำบาก",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_3",
                        text:"ไม่สามารถทำได้",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:4,
                question: "4) ท่านเดินได้มากที่สุดนานเท่าไร ก่อนที่จะมีอาการปวดเข่า",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_4",
                        text:"เดินได้เกิน 1 ชั่วโมง โดยไม่มีอาการอะไร",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_4",
                        text:"เดินได้ 6 - 60 นาที จึงเริ่มมีอาการปวด",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_4",
                        text:"เดินได้เพียง 5 นาที เริ่มมีอาการปวด",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_4",
                        text:"เดินได้แค่รอบบ้านเท่านั้น เริ่มมีอาการปวด",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_4",
                        text:"ทำไม่ได้และเดินไม่ไหว",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:5,
                question: "5) หลังทานอาหารเสร็จแล้วลุกจากที่นั่ง เข่าของท่าน มีอาการอย่างไร",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_5",
                        text:"ไม่มีอาการ",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_5",
                        text:"มีอาการปวดเข่า/ข้อเข่าฝืดเล็กน้อย",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_5",
                        text:"มีอาการปวดเข่าข้อเข่าฝืดปานกลาง",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_5",
                        text:"มีอาการปวดเข่ามาก/ข้อเข่าฝืด ลุกขึ้นยืนได้ด้วยความยากลำบาก",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_5",
                        text:"ปวดมากไม่สามารถลุกขึ้นได้",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:6,
                question: "6) ท่านต้องเดินโยกตัว (เดินกระโผลก กระเผลก) เพราะอาการที่เกิดจากเข่าของท่าน หรือไม่",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_6",
                        text:"ไม่เคย",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_6",
                        text:"ในช่วง 2-3 ก้าวแรกที่ออกเดินเท่านั้น",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_6",
                        text:"เป็นบางครั้ง",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_6",
                        text:"เป็นส่วนใหญ่",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_6",
                        text:"ตลอดเวลา",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:7,
                question: "7) ท่านสามารถนั่งคุกเข่า และลุกขึ้นได้ หรือไม่",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_7",
                        text:"ลุกได้ง่าย",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_7",
                        text:"ลุกได้ ลำบากเล็กน้อย",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_7",
                        text:"ลุกได้แต่ยากขึ้น",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_7",
                        text:"ลุกได้แต่ยากลำบากมาก",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_7",
                        text:"ลุกไม่ใหว",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:8,
                question: "8) ท่านมีปัญหาปวดเข่า ในขณะที่นอนกลางคืน หรือไม่",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_8",
                        text:"ไม่เคย",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_8",
                        text:"ใน 1 เตือนมี 1-2 ครั้ง",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_8",
                        text:"บางคืน 1 สัปดาห์ มี 1-3 ครั้ง",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_8",
                        text:"ส่วนมาก 1 สัปดาห์ มี 4-6ครั้ง",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_8",
                        text:"ทุกคืน",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:9,
                question: "9) ในขณะทำงาน หรือทำงานบ้านท่านมีอาการปวดเข่า หรือไม่",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_9",
                        text:"ไม่มี",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_9",
                        text:"น้อยมาก",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_9",
                        text:"บางครั้ง",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_9",
                        text:"ส่วนมาก",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_9",
                        text:"ตลอดเวลา",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:10,
                question: "10) ท่านเคยมีความรู้สึกว่าเข่าของท่านทรุดลงทันที หรือหมดแรงทันทีจนตัวทรุดลง หรือไม่",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_10",
                        text:"ไม่เคย",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_10",
                        text:"ในช่วงแรกที่ก้าวเดินเท่านั้น",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_10",
                        text:"บางครั้ง",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_10",
                        text:"ส่วนมาก",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_10",
                        text:"ตลอดเวลา",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:11,
                question: "11) ท่านสามารถเดินซื้อของใช้ต่างๆได้ด้วยตัวท่านเอง หรือไม่",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_11",
                        text:"ได้เป็นปกติ",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_11",
                        text:"ไปได้ เริ่มมีอาการปวดเข่า/ตึงเข่าเล็กน้อย",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_11",
                        text:"ไปได้ เริ่มมีอาการปวดเข่า /ตึงเข่ามากขึ้น",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_11",
                        text:"พอไปได้ แต่ด้วยความยากลำบากมาก",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_11",
                        text:"ไปไม่ไหว",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            },{
                key:12,
                question: "12 ) ท่านสามารถเดินลงบันไดไต้ หรือไม่",
                choice: [
                    {
                        key:"choice1",
                        group: "oks_12",
                        text:"เดินลงได้ เป็นปกติ",
                        point:4,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice2",
                        group: "oks_12",
                        text:"เดินลงได้ เริ่มมีอาการปวดเข่า/ตึงเข่าเล็กน้อย",
                        point:3,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice3",
                        group: "oks_12",
                        text:"เดินลงได้ เริ่มมีอาการปวดเข่า/ตึงเข่ามากขึ้น",
                        point:2,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice4",
                        group: "oks_12",
                        text:"เดินลงได้ด้วยความยากลำบากมาก",
                        point:1,
                        value: null,
                        checked: false
                    },
                    {
                        key:"choice5",
                        group: "oks_12",
                        text:"เดินลงไม่ได้",
                        point:0,
                        value: null,
                        checked: false
                    }
                ],
                score: null
            }
        ]
    },
    condition:(_score)=>{
        
        if(_score <= 19){
            return {
                type:'OKS',
                length:'0 - 19',
                name:'มีโอกาสเป็นโรคข้อเข่าเสื่อมระดับรุนแรง'
            }
        } else if(_score <= 29) {
            return {
                type:'OKS',
                length:'20 - 29',
                name:'มีโอกาสเป็นโรคข้อเข่าเสื่อมระดับปานกลาง'
            }
        }
        else if(_score <= 39) {
            return {
                type:'OKS',
                length:'30 - 39',
                name:'มีโอกาสเป็นโรคข้อเข่าเสื่อมระดับเริ่มมีอาการ'
            }
        }
        else if(_score >= 40) {
            return {
                type:'OKS',
                length:'40 - 48',
                name:'ปกติ'
            }
        }
        else {
            return null
        }
    }
}

export const assessments = [
    adl,
    oks
]

export default { adl, oks }
